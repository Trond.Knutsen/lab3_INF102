package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {

    final int CORRECT = 0;
    final int HIGH = 1;
    final int LOW = -1;

	@Override
    public int findNumber(RandomNumber number) {
        int upper = number.getUpperbound();
        int lower = number.getLowerbound();

        int middle = ((upper-lower)/2) + lower;
        
        return makeGuess(number, middle, upper, lower);
    }

    private int makeGuess(RandomNumber number, int middle, int upperBound, int lowerbound) {
        int lower = lowerbound;
        int upper = upperBound;

        int guess = number.guess(middle);

        if (guess == CORRECT) {
            return middle;

        } else if (guess == HIGH) {
            upper = middle;
            middle = (middle+lower)/2;
            return makeGuess(number, middle, upper, lower);

        } else if (guess == LOW) {
            lower = middle;
            middle = (upper+middle)/2;
            return makeGuess(number, middle, upper, lower);
        } else {
            return 0;
        }
    }
}