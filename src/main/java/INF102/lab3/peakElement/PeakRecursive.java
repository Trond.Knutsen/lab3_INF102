package INF102.lab3.peakElement;

import java.util.List;

public class PeakRecursive implements IPeak {

    @Override
    public int peakElement(List<Integer> numbers) {
        if (numbers.isEmpty()) {
            throw new IllegalArgumentException("Empty list");
        }

        if (numbers.size() == 1) {
            return numbers.get(0);
        }

        if (numbers.get(0) >= numbers.get(1)) {
            return numbers.get(0);
        } else {
            numbers.remove(0);
            return peakElement(numbers);
        }
    }
}
