package INF102.lab3.sumList;

import java.util.List;

public class SumRecursive implements ISum {

    @Override
    public long sum(List<Long> list) {
        
        if (list.isEmpty()){
            return 0;
        } else {
            long element = list.get(list.size()-1); //Get last element in list
            list.remove(list.size()-1); //Remove last element
            return element += sum(list);
        }
    }
}